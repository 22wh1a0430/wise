# -*- coding: utf-8 -*-
"""Wise_Module_2_Assignment.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/13ceq8F0YxOGfexlQHh_4ZM-_wP0EHaKx

# Assignments
1. Write a class Counter with increment and reset methods.

2. Write a class Calculator with all basic arithmetic operations like addition, subtraction, multiplication, division.
3. Create a class **Person** with instance attributes **name** and **age**. Add a class attribute **count** that keeps track of the number of Person objects created. Create multiple instances of Person and print the value of the **count** attribute.

4. Define a class **BankAccount** with instance attributes **account_number** and **balance**. Add a class attribute **interest_rate** that is set to 0.02. Add methods **deposit** and **withdraw** to modify the **balance**. Add a class method **get_interest_rate** that returns the value of the **interest_rate** attribute.

5. Create a class **Triangle** with instance attributes **side1, side2, and side3**. Add a method **is_equilateral** that returns True if the triangle is equilateral (all sides are equal) and False otherwise. Create instances of Triangle and test the **is_equilateral** method.
6. Create a class called **Rectangle** that has two attributes **width** and **height** and two methods **area()** and **perimeter()**. Implement these methods to calculate the area and perimeter of a rectangle. Then, create another class called **Square** that inherits from **Rectangle** and has a single attribute **side**. Override the __init__() method to set the **width** and **height** to be equal to **side**. Implement the methods to calculate the area and perimeter of a square.

7. Create a class called **Employee** that has two attributes **name** and **salary**. Implement a method called **calculate_bonus()** that calculates the bonus of an employee based on their salary. Then, create two classes called **Manager** and **Developer** that inherit from **Employee**. Override the **calculate_bonus()** method for both classes to calculate the bonus based on a different percentage for each class.
"""

#1.
from collections import Counter
print(Counter(['B','B','A','B','C','A','B','B','A','C']))
print(Counter({'A':3, 'B':5, 'C':2}))
print(Counter(A=3, B=5, C=2))

class cal():
    def __init__(self,a,b):
        self.a=a
        self.b=b
    def add(self):
        return self.a+self.b
    def mul(self):
        return self.a*self.b
    def div(self):
        return self.a/self.b
    def sub(self):
        return self.a-self.b
a=int(input("Enter first number: "))
b=int(input("Enter second number: "))
obj=cal(a,b)
choice=1
while choice!=0:
    print("0. Exit")
    print("1. Add")
    print("2. Subtraction")
    print("3. Multiplication")
    print("4. Division")
    choice=int(input("Enter choice: "))
    if choice==1:
        print("Result: ",obj.add())
    elif choice==2:
        print("Result: ",obj.sub())
    elif choice==3:
        print("Result: ",obj.mul())
    elif choice==4:
        print("Result: ",round(obj.div(),2))
    elif choice==0:
        print("Exiting!")
    else:
        print("Invalid choice!!")
 
 
print()

#4
class Bank_Account:
    def __init__(self):
        self.balance=0
        print("Hello!!! Welcome to the Deposit & Withdrawal Machine")
 
    def deposit(self):
        amount=float(input("Enter amount to be Deposited: "))
        self.balance += amount
        print("\n Amount Deposited:",amount)
 
    def withdraw(self):
        amount = float(input("Enter amount to be Withdrawn: "))
        if self.balance>=amount:
            self.balance-=amount
            print("\n You Withdrew:", amount)
        else:
            print("\n Insufficient balance  ")
 
    def display(self):
        print("\n Net Available Balance=",self.balance)
 
# Driver code
  
# creating an object of class
s = Bank_Account()
  
# Calling functions with that class object
s.deposit()
s.withdraw()
s.display()

#5
def checkTriangle(x, y, z):
 
    # _Check for equilateral triangle
    if x == y == z:
        print("Equilateral Triangle")
 
    # Check for isosceles triangle
    elif x == y or y == z or z == x:
        print("Isosceles Triangle")
 
    # Otherwise scalene triangle
    else:
        print("Scalene Triangle")
 
 
# Driver Code
 
# Given sides of triangle
x = 8
y = 7
z = 9
 
# Function Call
checkTriangle(x, y, z)

#6
class rectangle():
    def __init__(self,breadth,length):
        self.breadth=breadth
        self.length=length
    def area(self):
        return self.breadth*self.length
a=int(input("Enter length of rectangle: "))
b=int(input("Enter breadth of rectangle: "))
obj=rectangle(a,b)
print("Area of rectangle:",obj.area())
 
print()

"""## **Deficulty level: Intermediate**

### **Sales Person**

Suppose you are an owner of a company. You have several salespersons. They buy products from the factories and sell products to people.

* Create a class called Salesperson as follows:
* Each instance of the Salesperson class will hold the following information: name, record, and value
    * The name is a string, which is the salesperson's name.
    * The record is a dictionary of products in stock.
        - For the record, the key is the item name, and the value is a list with two elements ```[cost, quantity]```.
        - Cost is the cost of the item, quantity is the quantity of the item the salespersons have.
    * Value is a float number, which is their current profit/loss.

**Your task is to implement the following class methods:**

1. **```__init__(self, name)```:** the constructor creates a salesperson instance with a given name. After initialization, the record is an empty dictionary and the value is 0.
2. **```get_name(self)```:** returns the name of the salesman
3. **```buy_pro(self, product_name, market_price, quantity)```:** buy a product, ```(product_name, market_price, quantity)```, to owner's record. You need to update the record and value. If the product is in the record, update the values of ```record[product_name]```; if not, create a new key.
4. **```sell_pro(self, product_name, market_price, quantity)```:** sell a product, ```(product_name, market_price, quantity)```, to owner's record. You need to update the record and value. Update the values of ```record[product_name]```. After the update, if the quantity becomes zero, delete the key.
5. **```get_record(self)```:** returns the owner's product record.
6. **```get_value(self)```:** returns the value in the account.

### **Student Records**

You are given a file records.csv containing the data for few students. Kindly take a look at the file. In this question, you shall write a Python class called Records, which shall have four methods, and two member dictionaries The first dictionary shall be used to store student records, and the second a GPA scale:

1. **__init__(self, filename):**
This is the constructor method for the class. It should take the filename given as input (e.g. records.csv), and populate the first dictionary as follows:
    * the keys of the dictionary should be student names
    * the values of the dictionary should be a list, containing Department Year, Course1 grade, Course2 grade,Course3 grade, and Course4 grade. This is the information that appears in the file records.csv for each student. E.g. ```{'Jd': ['CS', 'Fresher', 'C+', 'D+' , 'F', 'D-'], ...}```

This method should also populate the second dictionary, a grade-to-numerical value map, whose keys should be the letter grades from A+ to F, and whose values should be the numerical GPA for the letter grade that Purdue has in its grading system, e.g.
```python
{
    'A+': 4.3,
    'A': 4.0,
    'A-': 3.7,
    'B+': 3.3,
    'B': 3.0,
    'B-': 2.7,
    'C+': 2.3,
    'C': 2.0,
    'C-': 1.7,
    'D+': 1.3,
    'D': 1.0,
    'D-': 0.7,
    'F': 0.0
}
```
2. **get_records(self, student):**
This methods should take a student name as input, and look up the record of this student in the first dictionary created by the __init__() method. If the student exists in the records, then it should return the student's record which will be a list. If the student does not exist, then it should return a string saying **"No record for <student> found!"** (replace **<student>** with its actually string value).
3. **insert_record(self, info):** 
This method should take a list containing a record for a student as input, e g. ```['Jason','Philosophy','Fresher','A','B','C','D']```, and insert it in the first dictionary prepared by the **__init__()** method. So after running this method, there should be a record for student Jason in dictionary, i.e. an entry like ```{..., 'Jason': ['Philosophy','Fresher','A','B','C','D'],...}```
4. **compute_gpa(self, student):**
This method shall take a student name as input. If the student exists in the first dictionary, then it should use the second dictionary, the grading scales, to compute and return the gpa for this student. Assume that each of the four courses whose grades are recorded are of 4 credit hours. If there is no record for the student passed as input, then the method should return a String saying **"No record for <student> found!"**.

### **Deck Of Cards**

Create a deck of cards class. Internally, the deck of cards should use another class, a card class. Your requirements are:

* The Deck class should have a deal method to pop a single card from the deck.
* After a card is dealt, it is removed from the deck.
* There should be a shuffle method which makes sure the deck of cards has all 52 cards and then rearranges them randomly.
* The Card class should have a suit (Hearts, Diamonds, Clubs, Spades) and a value (A,2,3,4,5,6,7,8,9,10,J,Q,K).

### **Bank Account**

Write a program to automate bank transactions (deposit and withdraw) by creating a class BankAccount to maintain account balance after every bank transaction. Minimum balance of Rs. 1000 must be maintained for any transaction. 
**Note: (Use Inheritance for Minimum Balance Check)**
"""